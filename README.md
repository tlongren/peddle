Peddle
---
**Contributors:** deviodigital  
**Tags:** theme, templates  
**Requires at least:** 3.4  
**Tested up to:** 4.3.1  
**Stable tag:** 1.0.2  
**License:** GPLv2 or later  
**License URI:** http://www.gnu.org/licenses/gpl-2.0.html

![Peddle WordPress theme for Easy Digital Downloads](http://www.robertdevore.com/wp-content/uploads/2015/09/peddle-image.png)

An Open Source WordPress theme for Easy Digital Downloads

## Description

This theme will allow you to utilize the Easy Digital Downloads plugin to sell digital goods on your WordPress website, in style.

## Live Demo &amp; Release Notes

You can view the theme in action on the [live demo](http://www.deviodigital.com/demo/peddle/)

Looking for more details behind the theme that aren't included in this readme doc? I've done a [write up](http://www.robertdevore.com/peddle-free-wordpress-theme-for-easy-digital-downloads) that gives a more thorough breakdown of the theme, it's built in options, and the road ahead.

## Installation

### Using The WordPress Dashboard

1. Navigate to the 'Add New' Themes Dashboard
1. Select `peddle.zip` from your computer
1. Upload
1. Activate the theme on the WordPress Themes Dashboard

### Using FTP

1. Extract `peddle.zip` to your computer
1. Upload the `peddle` directory to your `wp-content/themes` directory
1. Activate the theme on the WordPress Themes dashboard

## Changelog

### 1.0.2
* Added action hooks

### 1.0.1
* Added full width page template
* Added code to change the login page logo to your logo image uploaded through the customizer

### 1.0.0
* Initial release

## Development Information

This theme wouldn't have been possible without the following open source resources

* [Underscores](http://underscores.me/) starter WordPress theme
* [Bootstrap](http://www.getbootstrap.com) HTML/CSS framework
* [Easy Digital Downloads](https://www.easydigitaldownloads.com/) WordPress plugin
* [Font Awesome](http://www.fontawesome.io/) iconic font set
